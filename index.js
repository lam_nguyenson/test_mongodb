const mongoose = require('mongoose');
var Promise = require('promise');
const fs = require('fs');
const MONGO_URI = "mongodb://smarthome:smarthome2019@127.0.0.1/smarthome";

const HomeConfigSchema = new mongoose.Schema({
  name: {
    type: String,
    required: false
  },
  users: {
    type: [Object],
    required: true
  },
  home_environments: {
    type: [Object],
    required: true
  },
  serverversion: {
      type: Object,
      required: true
  }
}, {
  collection: 'home_config_temp'
});

if (process.env.NODE_ENV == 'development') {
  const HomeConfigs = mongoose.model('home_config_temp',  HomeConfigSchema);

  const connect = () => {
      mongoose.Promise = global.Promise;
      mongoose.connect(
        MONGO_URI,
        {
          keepAlive: true,
          reconnectTries: 100,
          useNewUrlParser: true,
          useUnifiedTopology: true
        },
        err => {
          if (err) {
            console.log(`*Error connecting to DB: ${err}`);
          }
          console.log('MongoDB Connected');
          fs.readFile('./data_json.json', 'utf8', function readFileCallback(err, data){
              if (err){
                  console.log(err);
              } else {
              obj = JSON.parse(data); //now it an object
              setInterval(()=>{
                console.log('insert');
                HomeConfigs.insertMany(obj).then((data) => {
                  console.log('insert finished');
                });
              }, 500);
          }});
        }
      );
      mongoose.set('useCreateIndex', true);
      mongoose.set('useFindAndModify', false);
    };
  connect();


  mongoose.connection.on('error', console.log);
  mongoose.connection.on('disconnected', connect);
} else {
  const forLoop = () => {
    return new Promise((resolve, reject) => {
        let data = [];
        for (let i = 0; i< 10000; i++) {
            const temp = {
                home_id: 'asdasdasdasd' + i,
                name: 'asdsadasdd' + i, 
                users: [{sdasd: 'asdasdsd'}],
                home_environments: [{eee: "asdsadsad" + i}],
                serverversion: { asdsad: "34324sdsd" + i}
            }
            data.push(temp);
            // console.log(temp);
        }
        resolve(data);
    });
  }


  forLoop().then((data) => {
      // console.log(data);
      const fs = require('fs');
      fs.writeFile("./data_json.json", JSON.stringify(data), function(err) {
          if(err) {
              return console.log(err);
          }
          console.log("The file was saved!");
      });
  });
}









