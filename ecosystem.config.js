module.exports = {
  apps : [{
    name        : "test-mongodb",
    script      : "index.js",
    args        : "",
    instances   : 4,
    exec_mode   : "cluster",
    watch       : false,
    out_file: "/dev/null",
    error_file: "/dev/null",
    autorestart: true,
    watch: false,
    max_memory_restart: '3G',
    env: {
      "NODE_ENV": "development",
    },
    env_production : {
       "NODE_ENV": "production"
    }
  }]
};
